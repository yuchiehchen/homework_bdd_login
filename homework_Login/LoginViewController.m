//
//  ViewController.m
//  homework_Login
//
//  Created by 陳郁傑 on 7/23/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import "LoginViewController.h"


@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UILabel *messagLabel;

@property NSString *username;
@property NSString *password;
@property NSString *message;

@end

@implementation LoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)checkVaildWithUsername:(NSString *)username password:(NSString *)password {
    return [_userService isVaildUsername:username password:password];
}

- (void)submit {
    if ([self checkVaildWithUsername:self.username password:self.password]) {
        self.message = @"Sucess";
    } else {
        self.message = @"Fail";
    }
    [self displayMessage:self.message];
}

- (void)enterUsername:(NSString *)username {
    self.username = username;
}

- (void)enterPassword:(NSString *)password {
    self.password = password;
}

- (void)displayMessage:(NSString *)message {
    self.messagLabel.text = message;
}

#pragma mark -
#pragma mark UI event

- (IBAction)enteringUsername:(id)sender {
    [self enterUsername:self.usernameTextField.text];
}
- (IBAction)enteringPassword:(id)sender {
    [self enterPassword:self.passwordTextField.text];
}

- (IBAction)pressedSubmit:(id)sender {
    [self submit];
}

#pragma mark -
#pragma mark For test

- (BOOL)checkDiaplayWithUsername:(NSString *)username {
    if ([self.username isEqualToString:username]) {
        return YES;
    }
    return NO;
}

- (BOOL)checkDiaplayWithPassword:(NSString *)password {
    if ([self.password isEqualToString:password]) {
        return YES;
    }
    return NO;
}

- (NSString *)getMessage {
    return self.message;
}



@end
