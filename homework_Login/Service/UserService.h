//
//  UserService.h
//  homework_Login
//
//  Created by 陳郁傑 on 7/23/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserService : NSObject

- (BOOL)isVaildUsername:(NSString *)username password:(NSString *)password;

@end
