//
//  ViewController.h
//  homework_Login
//
//  Created by 陳郁傑 on 7/23/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserService.h"

@interface LoginViewController : UIViewController

- (void)enterUsername:(NSString *)username;
- (void)enterPassword:(NSString *)password;
- (void)submit;

#pragma mark - Typhoon injected properties
@property UserService *userService;

@end

