//
//  main.m
//  homework_Login
//
//  Created by 陳郁傑 on 7/23/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
