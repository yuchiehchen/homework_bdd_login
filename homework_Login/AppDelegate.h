//
//  AppDelegate.h
//  homework_Login
//
//  Created by 陳郁傑 on 7/23/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

