//
//  CoreComponents.h
//  homework_Login
//
//  Created by 陳郁傑 on 7/31/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TyphoonAssembly.h"

#import "AppDelegate.h"
#import "UserService.h"
#import "LoginViewController.h"


@interface ApplicationAssembly : TyphoonAssembly


- (UserService *)userService;

- (UIViewController *)rootViewController;


@end
