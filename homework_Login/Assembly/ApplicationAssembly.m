//
//  CoreComponents.m
//  homework_Login
//
//  Created by 陳郁傑 on 7/31/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import "ApplicationAssembly.h"
#import "Typhoon.h"

@implementation ApplicationAssembly


- (UIViewController *)rootViewController {
    return [TyphoonDefinition withClass:[LoginViewController class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(userService) with:[self userService]];
    }];
}

- (UserService *)userService {
    return [TyphoonDefinition withClass:[UserService class]];
}

@end
