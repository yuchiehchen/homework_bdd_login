#import "Kiwi.h"
#import "LoginViewController.h"

@interface LoginViewController (LoginViewControllerTests)
- (BOOL)checkDiaplayWithUsername:(NSString *)username;
- (BOOL)checkDiaplayWithPassword:(NSString *)password;
- (NSString *)getMessage;
@end

SPEC_BEGIN(LoginViewControllerTests)
describe(@"A LoginViewController", ^{
    __block LoginViewController *vc;
    

    
    context(@"after user enter username and wrong password", ^{
        beforeAll(^{
            vc = [[LoginViewController alloc] init];
        });
        
        it(@"display user's typing words", ^{
            [vc enterUsername:@"eugene"];
            [vc enterPassword:@"XXXX"];
            
            [[theValue([vc checkDiaplayWithUsername:@"eugene"]) should] beTrue];
            [[theValue([vc checkDiaplayWithPassword:@"XXXX"]) should] beTrue];
        });
        
        context(@"and then user click the sumit button", ^{
            it(@"display login fail message", ^{
                [vc submit];
                
                [[[vc getMessage] should] containString:@"Fail"];
            });
        });
    });
    
    context(@"after user enter username and currect passwordand then user click the sumit button", ^{
        beforeAll(^{
            vc = [[LoginViewController alloc] init];
        });
        
        it(@"display login sucess message", ^{
            [vc enterUsername:@"eugene"];
            [vc enterPassword:@"1234"];
            [vc submit];
            
            [[[vc getMessage] should] containString:@"Sucess"];
        });

    });
    
});
SPEC_END
