//
//  homework_LoginUITests.m
//  homework_LoginUITests
//
//  Created by 陳郁傑 on 7/23/16.
//  Copyright © 2016 YuChiehChen. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface homework_LoginUITests : XCTestCase {
    XCUIApplication *app;
}

@end

@implementation homework_LoginUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    app = [[XCUIApplication alloc] init];
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [app launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_typing_vaild_username_and_password {
    
    // Arrange
    XCUIElement *username =  [app.textFields elementMatchingPredicate:[NSPredicate predicateWithFormat:@"placeholderValue like 'username'"]];
//    XCUIElement *password =  [app.textFields elementMatchingPredicate:[NSPredicate predicateWithFormat:@"placeholderValue like 'password'"]];
    XCUIElement *password = app.textFields[@"password"];
    XCTAssert(username.exists);
    XCTAssert(password.exists);
    
    [username tap];
    [username typeText:@"eugene"];
    
    [password tap];
    [password typeText:@"1234"];
    
    // Act
    XCUIElement *sumit = app.buttons[@"Submit"];
    [sumit tap];
    
    
    // Expect
    XCUIElement *message = app.staticTexts[@"Sucess"];
    XCTAssert(message.exists);
    

    
}

- (void)test_typing_invaild_username_and_password_async {
    
    // Arrange
    XCUIElement *username =  [app.textFields elementMatchingPredicate:[NSPredicate predicateWithFormat:@"placeholderValue like 'username'"]];
    //    XCUIElement *password =  [app.textFields elementMatchingPredicate:[NSPredicate predicateWithFormat:@"placeholderValue like 'password'"]];
    XCUIElement *password = app.textFields[@"password"];
    XCTAssert(username.exists);
    XCTAssert(password.exists);
    
    [username tap];
    [username typeText:@"eugene"];
    
    [password tap];
    [password typeText:@"eugene"];
    
    XCUIElement *message = app.staticTexts[@"Fail"];
    [self expectationForPredicate:[NSPredicate predicateWithFormat:@"exists == YES"] evaluatedWithObject:message handler:nil];

    // Act
    XCUIElement *sumit = app.buttons[@"Submit"];
    [sumit tap];
    
    
    // Expect
    [self waitForExpectationsWithTimeout:5 handler:^(NSError * _Nullable error) {
        NSLog(@"%@", error);
    }];
    
    
}

@end
